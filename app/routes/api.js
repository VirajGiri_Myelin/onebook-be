/**
 * Created by viraj on 23/04/2019.
 */
var User = require('../models/users');
var lastLogin = require('../models/lastloginout');
var config = require('../../config');
var jsonwebtoken = require('jsonwebtoken');
const shortid = require('shortid');
var secretKey = config.secretKey;

function createToken(user){
    var token = jsonwebtoken.sign({
        _id: user._id,
        username: user.username
    }, secretKey,{
        expiresIn : 60*60*24 // expires in 24 hours
    });
    return token;
}

module.exports = function(app, express, io) {

    var api = express.Router();

    api.post('/signup', function(req, res){

        var user = new User({

            FirstName: req.body.FirstName,
            MiddleName: req.body.MiddleName,
            LastName: req.body.LastName,
            Email:req.body.Email,
            BirthDate:req.body.BirthDate,
            Age:req.body.Age,
            Role:req.body.Role,
            username: req.body.username,
            password: req.body.password,
            Address:req.body.Address,
            Phone:req.body.Phone,
            Landmark:req.body.Landmark,
            Pin:req.body.Pin,
            isActive:true,
            isPaid:false
        });
        // var token = createToken(user);
        user.save(function(err){
            if(err){
                res.send(err);
                return;
            }
            res.json({
                success:true,
                message:"user has been created"
                // token:token
            });
        });

    });
    api.post('/login', function(req, res){
        User.findOne({
            username: req.body.username
        }).select('FirstName MiddleName LastName Role username password isActive').exec(function(err, user){
            if(err){
                res.send(err);
                return;
            }

            if(!user){
                res.send({message: "user not exist"});
            }else if(user){
                if(user.isActive) {
                    var validPassword = user.comparePassword(req.body.password);
                    if (!validPassword) {
                        res.send({message: "invalid password"});
                    } else {

                      var lstLogin   = new lastLogin({
                            loginTime: Date.now(),
                            loginUserId: user._id,
                            loginUserRole: user.Role,
                            logoutTime: req.body.logoutTime,
                            IPnumber: req.body.IPnumber,
                            userAgent: req.body.userAgent
                        });

                        lstLogin.save(function(err) {
                            if (err) {
                                res.send(err);
                                return;
                            }
                        });

                        var token = createToken(user);
                        res.json({
                            data: user,
                            success: true,
                            message: "successfully login",
                            token: token
                        });

                    }
                }else {
                    res.send({ message: "user not active"});
                }
            }
        });
    });
    api.post('/logout', function (req, res) {

        lstLogin.find({}).sort({$natural: -1}).limit(1).select('_id').exec(function (err, data) {
            if (err) {
                res.send(err);
                return;

            } else {
                lstLogin.updateOne({_id: data[0]._id},
                    {
                        logoutTime: Date.now()
                    },
                    {upsert: true}).exec(function (err) {
                    if (err) {
                        res.send(err);
                        return;
                    }
                    res.json({"success": true});
                });
            }
        });

    });

    api.use(function(req, res, next){

        console.log("user just come to our app");

        var token = req.body.token || req.param('token') || req.headers['x-access-token'];

        // console.log("user token",token);
        if(token){
            jsonwebtoken.verify(token, secretKey, function(err, decoded){
                if(err){
                    res.status(403).send({success: false, message:"fail to authonticate user"});
                }else{
                    req.decoded = decoded;
                    next();
                }
            });
        }else{
            res.status(403).send({success:false, message:"No Token provided"});

        }
    });

    api.post('/resetPassword', function (req, res){

        try {
            User.findOne({
                username: req.body.username
            }).select('name Role username password SchoolId').exec(function(err, user) {
                if (err) {
                    res.send(err);
                    return;
                }
                if (!user) {
                    res.send({message: "user dosent exist"});
                } else if (user) {
                    var validPassword = user.comparePassword(req.body.password);
                    if (!validPassword) {
                        res.send({message: "Invalid password"});
                    } else {
                        var newPassword = user.encryptPassword(req.body.newPassword );
                        User.updateOne({ username: req.body.username},{$set:{ password: newPassword }}, function(errpassword) {
                            if (errpassword) {
                                res.send(errpassword);
                                return;
                            }
                            res.json({
                                status: "success",
                                message:"Password Changed successfully"
                            })
                        });
                        // res.send(user);
                    }
                }
            });
        }

        catch (exception) {
            res.send(exception);
            return;
        }
    });



    return api;

}