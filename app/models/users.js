/**
 * Created by viraj on 23/04/2019.
 */
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;
var UserSchema = new Schema({
   
	firstName:{type:String, require:false},
	middleName:{type:String, require:false},
	lastName:{type:String, require:false},
	birthDate:{type:Date},
	age:{type:String},
	role:{
        type: String,
        require:true,
        enum: ['guest','user','storytellor','admin']
    },
	isActive:{type:Boolean},
	isPaid:{type:Boolean},
    email:{type:String, require:false},
    address:{type:String},
    phone:{type:String},
    landmark:{type:String},
    pin:{type:Number},
	username:{type:String, require:true, index:{unique:true}},
	password:{type:String, require:true, select:false},
	experience:{type:String},
	voiceType:{type:String}
});


UserSchema.pre('save', function(next){
	var user = this;
	if(!user.isModified('password')){
		return next();
	} 
	
	bcrypt.hash(user.password, null, null, function(err, hash){
		if(err) {
			throw err;
			return next("I am in err",err);
		}

		user.password = hash;
		next();
	});

});
UserSchema.methods.encryptPassword = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
}
UserSchema.methods.comparePassword = function(password){
	var user = this;
	 return bcrypt.compareSync(password, user.password);
}

UserSchema.plugin(uniqueValidator);
module.exports = mongoose.model('User', UserSchema);
