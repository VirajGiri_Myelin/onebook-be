/**
 * Created by viraj giri on 27/06/2019.
 */
var mongoose = require('mongoose');
const shortid = require('shortid');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;
var logInOutSchema = new Schema({
    loginTime:{type: Date},
    loginUserId:Schema.Types.ObjectId,
    loginUserRole:{type:String, require:true},
    logoutTime:{type: Date},
    loginIPAddress:{type:String},
    userAgent:{type:String}

});
logInOutSchema.plugin(uniqueValidator);


module.exports = mongoose.model('lastLogInOut', logInOutSchema);