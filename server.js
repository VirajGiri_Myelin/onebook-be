var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var config = require('./config');
var mongoose = require('mongoose');
var socketApi = require('./socketApi');

mongoose.Promise = global.Promise;
mongoose.connect(config.database, {useNewUrlParser: true }, function(err){
	if(err){
		console.log(err);
	}else{
		console.log("database connected");
	}
});


var app = express();

var io = socketApi.io;

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended:true}));
app.use(bodyParser.json());
app.use(bodyParser.json({type: 'application/vnd.api+json'}));
app.use(morgan('dev'));

var api = require('./app/routes/api')(app, express,io);
app.use('/onebook',api);



/*
app.get('*', function(req, res){
	res.sendFile(__dirname + '/public/views/index.html');
});
*/
app.all('/*', function (req, res, next) {
    // CORS headers
    res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    // Set custom headers for CORS
    res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,x-access-token');
    if (req.method == 'OPTIONS') {
        res.status(200).end();
    } else {
        next();
    }
});

if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.json({
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.json({
        message: err.message,
        error: {}
    });
});






module.exports = app;