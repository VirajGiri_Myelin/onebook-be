var socket_io = require('socket.io');
var io = socket_io();
var socketApi = {};

socketApi.io = io;

io.on('connection', function(socket_io){
    console.log('A user connected');
  /*  socket_io.on('story', function(data){
        io.sockets.emit('story', data);
    });*/

    socket_io.on('end', function (){
        socket_io.disconnect(0);
    });
});
module.exports = socketApi;